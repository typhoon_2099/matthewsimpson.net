---
layout: post
title: "Using Bugsnag with react-native-web"
date: 2021-01-16 16:00:00
categories:
  - Blog
tags:
  - Jekyll
  - Bugsnag
excerpt: Using Bugsnag with react-native-web isn't officially supported, but it's pretty easy to set up
image: /images/tools.jpg
---

## Not available yet

https://github.com/bugsnag/bugsnag-js/issues/1038

## Set up Bugsnag with expo

## Install ReactJS Bugsnag libs

## Add both to namespaced components

## Use in you App.js file as before
