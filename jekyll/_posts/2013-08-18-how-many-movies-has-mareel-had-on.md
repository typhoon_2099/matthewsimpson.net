---
layout: post
title: How many movies has Mareel had on?
date: 2013-08-18 17:00:00
categories:
  - Blog
---
I got bored and decided to fire out a quick table to display how many films have been on in Mareel since the 21st of June (since I started collecting the data).

Here are the results:

| Title                                              | Count |
| -------------------------------------------------- | --    |
| Monsters University                                | 88    |
| Despicable Me 2                                    | 86    |
| The Smurfs 2                                       | 41    |
| The World's End                                    | 36    |
| World War Z                                        | 36    |
| Percy Jackson: Sea of Monsters                     | 20    |
| Now You See Me                                     | 19    |
| Before Midnight                                    | 14    |
| Behind the Candelabra                              | 13    |
| Pacific Rim                                        | 13    |
| The Heat                                           | 11    |
| Much Ado About Nothing                             | 8     |
| The Wolverine                                      | 8     |
| Man of Steel                                       | 7     |
| Planes                                             | 7     |
| The Lone Ranger                                    | 5     |
| Springsteen & I                                    | 3     |
| Summer In February                                 | 3     |
| Thérése Desqueyroux                                | 3     |
| A Field in England                                 | 2     |
| Aguirre: The Wrath of God                          | 2     |
| ASN Friendly Screening: E.T. The Extra-Terrestrial | 2     |
| Byzantium                                          | 2     |
| Caesar Must Die                                    | 2     |
| Fast & Furious 6                                   | 2     |
| Mud                                                | 2     |
| Rivers and Tides                                   | 2     |
| The Bling Ring                                     | 2     |
| The Conjuring                                      | 2     |
| The Iceman                                         | 2     |
| This is the End                                    | 2     |
| We Steal Secrets: The Story of Wikileaks           | 2     |
| ASN Friendly Screening - Monsters University       | 1     |
| ASN Friendly Screening: Despicable Me 2            | 1     |
| ASN Friendly Screening: The Smurfs 2               | 1     |
| Autism Friendly Screening - Monsters University    | 1     |
| Autism Friendly Screening - The Lone Ranger        | 1     |
| Epic                                               | 1     |
| Our Children                                       | 1     |
| The Audience                                       | 1     |

Kind of interesting. The summer has definitely been aimed towards the kiddly-winks from the looks of it. Thankfully there have been a good selection of films on in the last 2 months to enjoy, a feat that's even more impressive considering there are only 2 screens in the cinema.

I might do a couple of other tables (perhaps comparing 2D vs 3D) so keep an eye out for that popping up some time soon.
