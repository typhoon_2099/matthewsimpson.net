---
layout: post
title: Kamen Rider Fourze Henshin Sound
date: 2013-05-02 17:00:00
categories:
  - Blog
---
I made a ringtone of Fourze's henshin sound.

[Here's the file, enjoy](/files/Fourze_Henshin.ogg) (right click and select "Save link as...")
