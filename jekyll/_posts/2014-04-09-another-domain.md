---
layout: post
title:  "I Got Another Domain"
date:   2014-04-09 16:00:00 +0100
categories:
  - Blog
---
I picked up another domain today since it was on sale.

I've managed to get myself [matthewsimpson.co](http://matthewsimpson.co/), which I got on sale for £9.99 (ex. VAT). Next year it'll be £19.99, but that's not too bad. I'd really love to get [matthewsimpson.com](http://www.matthewsimpson.com/), but it's a bit pricey for me (I can purchase it for a bargain at £1200!). I'll stick with a domain that's almost exactly the same, just one character less.

P.S. If you try going to the domain you'll be redirected to [matthewsimpson.net](http://matthewsimpson.net/), which I did by modifying the htaccess in the web root of this site.
