---
layout: post
title: Writing Prompt One - The Hike
date: 2016-12-21 22:00:00
categories:
  - Writing
tags:
  - "Rory's Story Cubes"
  - "Writing Prompts"
excerpt: I picked some Rory's Story Cubes and decided to use them for writing prompts. Here's the first story.
---

I was doing some Christmas shopping and I came across a toy/game/creative aid called [Rory's Story Cubes](https://www.storycubes.com/), which you can [find on Amazon](https://www.amazon.co.uk/d/Shops/Creativity-Hub-Rorys-Story-Cubes/B003NFJMBM). The idea is simple, you have 9 dice with different icons on each side, roll them and use the icons as prompts for your story.

I've seen Rory's Story Cubes before but never had a proper shot of them so I picked up a set for myself (don't worry, I did buy the Christmas shopping too!). I'll be taking them with me to the next few social events I attend (and I feel like there's a way to use them in my improv too...), but for now I'm going to use them as writing prompts to inspire some short bits of writing. Here's the first one I've written, I've put the icons beside the words they were used to inspire.

Oh, and it's probably worth noting that I wrote this all in one go. I haven't done a second draft of this, it's purely what came to mind from the prompts I was given.

---

It was getting dark and Matthew had {% include cube.html src="compass" %} **NO IDEA WHERE HE WAS**. He'd been out hiking in the woods but had lost track of time. By now it was starting to look as if he would have to try and find somewhere to set up his tent and stay for the night. If his {% include cube.html src="phone" %} **PHONE** had a signal or the trees hadn't covered so much of the sky he might have been able to use his GPS to get out, but it was too late now.

It was a {% include cube.html src="turtle" %} **SLOW TREK** through the woods, the trees were dense and the terrain was uneven. This was compounded by the fact that there was very little light to go by; Even without the canopy overhead there wasn't much moonlight to go by, it being only a couple of days since the last {% include cube.html src="crescent_moon" %} **NEW MOON**.

Suddenly Matthew came upon a spot where he could pitch his tent. The ground was flat and soft here; The pegs would drive into the ground without any issue. He set down his backpack and pulled out the tent, then set to work erecting it. The darkness made the task more difficult than it should have been, but thankfully his phone's torch did a good enough job of highlighting the trickier moments.

With the tent set up an Matthew finally having a moment to rest he suddenly realised how {% include cube.html src="apple" %} **HUNGRY** he had become. He'd been trekking for a few hours now and the thought had never crossed his mind to stop for a snack. He'd heard from the locals in a nearby village that the views from the top of hill once you'd made your way through the forest were {% include cube.html src="rainbow" %} **MAGNIFICENT**, so has instead elected to soldier on instead.

Matthew checked the bag. "Oh, no!", he thought, "where did my food go? Did I forget to pack my food?". The backback was devoid of sustenance. What would he do? Maybe there's something he could find nearby, he'd just have to go looking around. Before he left the tent though he'd need to figure out how to mark his way back.

As a shiver ran up Matthew's back an idea sprung to mind "Of course, I'll build a {% include cube.html src="fire" %} **CAMP FIRE**!". It should have come to him sooner, but his hunger was starting to cloud his sensibilities. He'd make sure to eat plenty before he went to sleep tonight! Looking around the campsite Matthew found enough stones to make a circle to place some firewood into, then grabbed a few loose twigs and logs to pile into the middle. Thankfully it was dry enough that his lighter managed to set the leaves at the bottom burning relatively quickly.

Once the fire has settled into a steady burn (and he had warmed himself up a bit, the night was getting cold!) Matthew set off in search of something to eat. Without enough knowledge of mushrooms and fungi he'd decided to forgoe collecting these, the last thing he wanted was to poison himself in the middle of knowhere! It didn't take long before he found an apple tree with some decent sized apples growing on them. He didn't know what type of apple they were but at this point he was too hungry to care. The red-green, shiny skin on the apple look to delicous to deliberate over it's exact title.

Matthew ate one of the apples, then grabbed 3 more before he started making his way back to the tent. It was then that he noticed a noise he hadn't heard until now. In the distance he could hear {% include cube.html src="fountain" %} **RUNNING WATER**. It sounded like a stream, and it wasn't too far away. Since he was unsure how much farther his trek would take him, Matthew thought it would be best to go to it and fill up his water bottle, which was now starting to run dry.

Matthew got to the stream and took out his bottle. Holding it under he filled the bottle to the lid, then closed it and put it back in the side pocket of his backpack. The trees were clearer around the stream, and Matthew could finally see the stars which were out in force. So far away from any light pollution he could see the milky way with his bare eyes, a sight he'd been longing to see for a long time, ever since he'd {% include cube.html src="house" %} **MOVED TO THE CITY** all those months ago. Ursa Major, Cassiopeia, Orion, all the constellations he'd missed being able to see from his bedroom window, were now right in front of him.

Matthew lay down on the ground beside the stream and looked up for a while. This was the moment he'd set out to find, the views from the hill played second fiddle to the glory of the constellation in his mind. Reaching the top didn't matter anymore; In the morning he'd pack up his tent and make his way back down, then home to the city to dream about the stars again.

THE END
