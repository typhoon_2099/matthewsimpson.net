---
layout: post
title: Project Octagon - My Experience Commemorating the First Day of the Battle of the Somme
excerpt: "This Friday (the 1st of July, 2016) I got the opportunity to perform in Project Octagon, a nationwide art performance commemorating those that fell on the first day of the Battle of the Somme."
date: 2016-07-03 17:00:00
image: /images/project_octagon.jpg
image_alt: Project Octagon
categories:
  - Blog
---
## Introduction

This Friday (the 1st of July, 2016) I got the opportunity to perform in a countrywide art performance. Hopefully you saw it; the performance ran for a full 12 hours and culminated at 8pm. It was designed to hit as many populated areas as possible and create a sense of unease in the public. It was also announced via multiple nes outlets (national newspapers as well as the BBC and prominent social media celebrities).

A wanted to write about my experience on this project, both as a way of expressing my gratitude for having had the opportunity to be involved in such a major production and as a way of documenting my experience.

## The initial pitch aka the mystery offer
My involvement in this project began near the end of April when Chris Grant approached us at one of our Improv rehearsals. He was doing the rounds trying to find as many groups as he could that he could invite into coming to the first session and hopefully commit to the project. The only problem was that he wasn't allowed to tell anyone what the project was about.

Chris started by telling us that the project was for males aged 18-41, so Marjolein, Ashley and Les had to go and wait outside. He then proceeded to give us as much information as he could, which turned out to be very little. All we found out was that we'd be involved in a nationwide art performance, that would happen some time in July, and would be a one-off event that would hopefully have a large impact on the public.

The mystery behind the project had me intrigued, so there was no way I wasn't going to turn down the opportunity to participate. I only had to go to the first session, if it turned out to be some weird dance performance down at the Market Cross then I could say no thank you and walk away. The rest of the (eligible) Imposters were as intrigued as I was so that was 5 of us baited and on the hook, ready to find out what we'd actually agreed to.

## The First Session

A few weeks later I got a message on Facebook to come to the TA Hall at Fort Charlotte for the first session. Here we'd do some exercises then be told what the project was actually about.

The first half of the session was a set of warm ups and team building/focusing exercises, designed to get us into a common base for our performances. Once we'd finished these and had a bottle of water and a banana we sat around the project coordinators to find out what we were hopefully going to commit to.

Not so fast! Before we could be told anything we had to sign Non-disclosure Agreements (or an NDA, if you will). This was my first time having to sign an NDA, and my mind started racing through all the possibilities of what we were about to find out. Once I signed the document I'd be part of the big secret.

It was somewhere around here that Ashley came into the fold. My fellow Imposter was also part of the project! It appears I've already been oblivious to the project, as by this point it must have been going on for weeks in various stages of development. Now I had to know, and I definitely wanted to be a part of the project. It started to feel like this was something that I'd remember for a long time to come.

Once everyone had signed their forms and handed them back we got given the brief; If we chose to go ahead an commit to the project we'd be spending the entire day of the 1st of July walking around town and hanging out dressed as world war one soldiers to commemorate the first day of the Battle of the Somme, exactly 100 years later. Not only that but we were each going to be assigned a soldier who was killed on that day. We'll be given some information on our soldiers to give to the public on a card should such an opportunity arise. This was a big deal. It would have been an amazing project to work on even if it was just soldiers that we were playing, but the fact that we were to be playing real soldiers, actual people who went to the Somme and died for their country, added so much gravitas to the project. I was 100% invested now.

## The Team Building

The team building exercises were pretty interesting. They were designed to train us to be able to sense queues rather than being given them. By that I mean that they worked on our awareness of the space around us and what is going on without necessarily having to be constantly looking around the room.

The first part of each of the physical exercises was the warm up. Just the usual fare here, rotate the joints to make sure nothing gets sprained. The second part was something new to me. In our circle we would follow a movement from Chris (Wright) and try to hold it until he moved into the next position. The weren't easy positions either. Sometimes it was crouching on one knee (the other leg is stretched out in front of you), sometimes it was getting into a plank position. The worst by far was resting in a half push-up position. Try it, it's horrible.

The follow the leader movements eventually progressed into a Mexican wave style game, where Chris would get into a position and we'd go around the circle moving into position. The goal of this was to help us focus on our movements and being more aware of our surroundings. It's a fun one to try out, but again when you're stuck in an awkward position waiting for someone else to twig on that they're supposed to move then it gets pretty frustrating!

For our main exercises we did two things. The first one consisted of us passing a ball around our circle, throwing it to anyone who hadn't already had it. Once that was done we started moving about the room and passing the ball around in the order we did before. To complicate it we then added an object to pass around to someone else (we used a roll of bubble wrap), and then added actions to do once we'd passed on the ball or the object, such as sitting down or doing a push-up. All of this seems daunting when you look at the final product but stepping up to it makes it a lot easier. Again we were training our ability to focus, and it's surprising when you realise how automatic some of the exercise becomes after a while.

Our second main exercise was a movement exercise where we would move around the room and respond to certain calls. We had to either start walking in a line, move into the middle of the room, go to the side of the room and touch the wall, or all move into the middle of the room and stand shoulder to shoulder with each other. After a while the calls were removed and we started reacting to others in the group. We did this every week and by the end of it we were reacting to subtle queues so well that it really started to feel like we had achieved some sort of group mind.

There were other exercises we performed but these are the two main ones and the ones that stick in my mind most vividly.

## The Acting

For our performance we were going to be hanging out in different areas around Lerwick, so we had to practice how we were going to achieve this. The areas would be a range of sizes and shapes, so we needed to be able to enter a space and come to rest in a spacing that seemed as natural as possible. We ended up working on three formations: Tight clump, loose clump and strung out.

Tight clump and loose clump are pretty similar, tight clump has us all in a relatively close space whereas the loose clump we could be spread out over a much larger area. With loose clump we'd also try to keep in groups of two or three, with maybe a couple of people on there own.

The strung out formation would be used in long areas, such as along pavements and down streets. The group would peel off as we enter the area into distinct clumps. In addition, the clumps would start with a single person, and increase as you go through the area. The effect for the public will be one of a gradual increase in our presence, and hopefully help with creating that sense of unease.

Our practising was extremely useful. It sounds like an easy task; Move into and area, find a space, and ensure about half are sitting and the other half are either standing or leaning. It turns out it's not so easy. The first time through there was a lot of unease with the group. There was also a lot of looking around and counting how many are standing and how many are sitting. The end result is a very unnatural looking tableau.

As we continued our practising we concentrated on being able to rest in the space without having to look around so much, as well as being able to move to balance the tableau without it looking like we're reacting to someone else's movement. The end result looked much more natural. Nobody seemed uncomfortable or out of place. We only practised for a few minutes at a time but on the actual day we would be in one area for 20-25 minutes, so we needed to be okay with holding a position for a long time. If we all kept moving about we'd end up looking too staged.

## The Costume

During the project we were told about the costumes we would be wearing. Initially Chris mentioned that the costumes would be authentic World War One uniforms, and people would be split between wearing trousers and kilts (for the Gordon Highlanders). I most definitely wanted a kilt.

The costumes didn't arrive until the 4th or 5th week, but Chris kept us up to date on the progress. He mentioned seeing them all laid out when they arrived (something I believe Cara, Letty and Ellie had a monumental job in sorting all the clothes into sets, and assigning each one to each of us, based on the measurements they took on the first session. I don't envy those women, it must have been a nightmare.

Before we got the chance to try on the costumes then Chris passed on some information he'd relieved about the costumes; Not only were these costumes being made specifically for this project byt a company in Poland, but some of the costumes had been used between their manufacturing and now in the new Wonder Woman movie that's coming out next year! This meant that, if we tried on our costumes and we found any dirty marks on then, there would be a good chance that the costume we were wearing had been in the film!

I went along on a Tuesday night in the week to try on my costume and make sure everything was alright with it. It was this night that I found out that I'd been assigned to the Gordon Highlanders, so I was getting a kilt! The only downside to wearing a kilt is that it really adds to your waistline, so the assigned jacket didn't fit comfortably. Cara managed to magic up a slightly bigger jacket for me in mere moments, so I got myself dressed up and then admired the result.

I did realise that by changing jackets I had also changed soldier, as the names were attached to each. I feel kind of bad for the person I had to give up, this was a chance to celebrate this person and the sacrifice they made, and I had to set him to the side because I decided I wanted a chippy a few days before. Hopefully someone else had managed to eat a bit healthier than me since the measurements were taken so that he could regain his place in the project.

Unfortunately I couldn't find any dirt on my costume, the thing was spotless. So no action movie anecdotes for me! On the positive side the costume was impeccable. I spent some time admiring myself in the mirror, I was starting to feel like a real soldier. The accuracy of the costume would really help with our performances on the day, and in making the public believe who we are. It's also really going to help towards us looking out of place, something the artist wants to achieve.

The costumes needed to be broken in. The boots especially had absolutely no give in them, so there was a lot of work to be done. We couldn't use the boots in the TA hall for fear of damaging the floor, but we did manage to get outside with the ona  couple of sessions. The rest of the costume we broke in by wearing them as we did some physical challenges. We did a few races down the hall either crawling or leaping or running around like a cat. Considering the costumes were made of wool then it ended up being an incredibly uncomfortable experience!

## The Cards

During our performance we were going to be giving out cards to the public. These card would contain information about the soldier we are portraying, such as they're name, rank, date of birth and the date that they died (which for all the soldiers as 1st July 1916). There was going to be 200 for each person, and we were encouraged to give out as many as possible to the public. This would come in handy as a simple method of preventing too much public intervention.

We'd practised some methods to stop the public becoming a problem but these were last resort measures really. The majority of people would be happy with the cards, which also included the hashtag #wearehere, allowing the public to share their experiences and find more information about the event.

## The song

As part of our performance we would have a song to sing. The song would be sung randomly throughout the day, and be triggered by the group leader. They would sing the first couple of lines and then everyone else would join in. As the song goes along we would build up the volume, so that when the song ends we'd be left with an unsettling silence. It was also important to remain where we were for a moment, and leave just after the song. This would help with the unease in the public.

The song was one that got sung by soldier throughout the war. The words were "We're here because we're here, because we're here, because we're here", and was sung to the tune of Auld Lang Syne. It's a very powerful song for the day. Lots of soldiers ended up on the front lines, and I think a lot of them genuinely would not have know fully what they were there for, just that they were required to fight for their country. It's the perfect song for the day.

## The Haircut

As part of appearing as authentic as possible then we were offered free haircuts so that we could match the style of the time. For the majority of people there was no need to change, as long as the hair was short enough it looked fine. A few of the other members of the group had much longer hair so this was going to have to be restyled. I sat somewhere in the middle of the two, my hair was short enough, but I felt like I really wanted to commit to looking as authentic as possible, so I got a haircut.

Usually my hair is pretty messy (but in a controlled way). It's also extremely curly so many years ago I decided it was easier to keep it a bit long and throw in some Brylcreem to stop it going absolutely mental. I decided to drastically change my hair to a regulation cut, which is short on the back and sides, and is waxed and combed to the side. Due to the strength of my curls I've had to use a fair but of wax to control it.

The end result is fantastic. The hair cut looks great, and not even that out of date really. Once I put the uniform back on and had a look in the mirror I got really excited. Now I really felt like a World War One soldier.

## The Final Week

For the final week we increased the sessions. Usually we had the chance to come to either the Sunday or the Monday session, this week we were encouraged to come to both. We also had a final meeting on the Wednesday night to go over the final bits and pieces, and get the start times for our groups so we knew when we would be starting our day. Our group were to arrive at The Garrison and 7:15am and be ready to leave at 8:30am.

It was only during this final week then the nerves started kicking in. We'd done weeks of practising up to this but it was only now that I started considering how all that training was going to fit into the final performance. It's also when I started wondering if I was going to be able to pull this off, I was sure that somehow I would manage to mess something up, but had no idea what it would be.

## The Performance

We started our day early (I was up at 6am) and got into costume. Everyone at the Garrison was excited or nervous about how our day was going to go. Once we were in our uniforms we were giving a healthy covering of grease and dust to make our costumes look properly worn. It was the icing on the cake, the costumes were as realistic as they could possibly get. All greased up, I went down into the main theatre to find out about our route.

Our route for the day had us moving about Lerwick all day. Other groups were heading to the Northlink Ferry, Scalloway and St. Ninians, which sounded like amazing opportunities to meet tourists and generally to impact the public as much as possible. Our day was split into four parts, with breaks at the Garrison in between.

Our handlers for the day were Amanda Shearer and Lorna McKay. They were responsible or signalling us to stop and move on, as well as ensuring that everyone was safe on the day. Their help was invaluable.

### Part 1

We started at 8:30am and headed down to the Bressay Ferry Terminal. once the ferry had come in and boarded the cars and passengers then we went aboard and took a trip to Bressay and back. We almost missed our boarding, while we were waiting then the ferry crew started closing the gate, so we all made our move and got on board.

The trip was interesting. It was our first public interaction so I was curious to see their reaction and how we handled it. For the most part people seemed to be too preoccupied with going to work. I'm guessing that some people also didn't know what was going on or wanted to approach us. It was also raining so I don't think people were too keen on getting out of their cars to investigate.

From the ferry we moved down to the taxi rank on the pier, where a lot of cars were slowing down to looking at us, trying to figure out what we were up to. A tourist came up to me and asked if what we were doing so related to the Somme, so I handed him a card. He read it and thanked me, then went on his way. My first interaction had gone smoothly, so I was pretty pleased.

Next we moved onto the Market Cross. Another of the three groups met up with us and we hung out while even more tourists came along, taking photos and enquiring to the silent soldiers about what we were up to. One of my highlights of the day was here when we started singing. Both groups singing added so much volume so when we finished and left that eerie silence at the end it really hit you.

After the Cross we moved down to the lifeboat pier for a while and then walked up and down the lanes just of Commercial Street and sang the song another couple of times. The lanes are quite narrow so our singing became much louder, and echoed down to the street and probably well beyond. Once we were back on the street there were lot of tourists gathered around to get photos.

We finished off the first quarter at the Town Hall, just beside the Garrison. Someone from the council came and go some photos to post on Twitter, then we hung around near the entrance to the building for a while.

### Part 2

After our first break (where I threw off my jacket to try and cool down and dry off) we headed back out and down to the Toll Clock Shopping Centre. We all got into positions around the Skippydock Cafe, right in the middle of the Toll Clock. I noticed a couple of our group go and sit down with people eating, and one of the cooks came out to get her photo taken with our tallest soldier. She was pretty short and was loving the height difference!

We moved on and headed to the Shetland Museum and Archives. Outside we got out our rations (a cheese sandwich and an apple) and ate them near the entrance.

While we were outside the museum a tourist asked one of the soldiers what we were doing. She was handed a card and, once she'd read it, turned to her husband and said "ah yes, it's for the first day of the battle of the Somme." There was something about how she said this that really hit me. She was quite melancholy, and it was the first time in the day that I really felt how moving what we were doing could be to the public.

We went inside the museum for a while and, after confusing the people who worked there, moved on to outside Mareel. We were in a quieter area, just outside of the cafe, but we passed by the window so those that were inside definitely saw us go by. Again we hung around for a while then moved on to the Anderson High School, via the street.

On our way to the Anderson we walked along the street and passed by another group that were in a strung out formation. This was a really great moment, lots of people had gathered on the street to see these guys in there tableau and then suddenly we came marching through with no warning (apart from the clip clopping of our metal tacked boots). The result must have been really effective on the public, seeing so many soldiers in one location.

Once we go to the Anderson we did a tableau outside the Bruce Hostel then headed inside to site around the sixth year pupils while they had lunch. Most of the children either weren't too fussed about us, confused or already knew about us from social media. Clearly our performance through the day up to this had been making it way around Facebook, Twitter and Instagram as intended. We also had a teacher with us, so some of the pupils and teachers got an excitement from seeing him turn up (he gave out quite a few cards at the school).

We sang our song before we moved on. It was another fun location to do this as the pupils suddenly became silent. If you've been in there before you'll realise how loud they can get, and once we'd finished you could almost have heard a pin drop.

### Part 3

Once we left the Anderson we went for a walk around the Knab and down to the Sletts. We stopped around the back of the medical centre and did a tableau there. We weren't there for long before heading to the Clickamin.

It was around this point that my knee started getting sore. I don't know what I did but going down stairs or downhill was starting to become quite painful. There was no way I was bailing out, I was going to march on.

At the Clickamin we went into the swimming pool area. This may have not been the smartest move, we were already sweating heavily from all the wool we were wearing on what had become quite a warm day (even the rain had stopped at this point). A few of the other soldiers went down to the gym to see if there were people there. After managing to wrangle us all back together we headed back to the Garrison to get some lunch.

### Part 4

After a healthy quantity of soup and sandwiches we headed back out and took a long route back to the Clickamin. We stood outside this time near the main entrance. It had started raining again but there were still quite a few people that stopped to look at us and take photos and get cards from us.

We moved on and walked around the loch towards the Clickamin Broch. Before we got there though, we did a strung out formation on the pavement up to the entrance. It seemed to be really effective, people passing us seemed to enjoy it and there were even people across the road that came across to get some photos. Once we were ready we moved down to the Broch.

The Clickamin Broch was pretty quiet, but I was glad to let my knee rest for a bit. Thankfully this last route was a relatively flat one, so I didn't feel too much pain through it. After a while we gathered up and moved on to Tesco.

Tesco was fun. We all separated and spread ourselves around the supermarket. The public had a range of responses. A lot of people tried to ignore us and get on with their shopping. Quite a few were interested in what was going on. We handed out as many card as we could before we went outside to do a tight clump near the entrance.

The last place we visited before heading back to the Garrison for the final time was Islesburgh. We sat inside the Cafe for a bit before heading outside and spreading ourselves along the road outside the building. After a while we moved on to get some food before the finale of the day.

## The Finale

After quite possibly the best curry I've every had we got organised and walked down to Mareel. For the finale all three groups were heading there for maximum impact. We all entered the cafe/bar and filled up the space as much as possible. This was at half 6 in the evening so it was pretty busy, which meant we got to get even closer to the public than we had before.

NOTE: Before reading the rest of this chapter then please watch [this video](https://www.youtube.com/watch?v=PeMt95ktxiI) to see the final performance, it's a fantastic clip that shows off the full power of the finale.

After we'd been in the cafe/bar for a bit the signal was given for us to move into the foyer. Here we walked up and down the space in circles, using as much of the space as possible. Once the next signal was given we found spaces to come to rest and stayed still. This wasn't the same as our previous tableaus, this time we were being as still as possible, no fumbling around, no swapping between standing and sitting, and no moving to other spaces.

The last signal came for Andy to start singing the song for the last time. All three groups singing the song simultaneously was so strong. It took me a moment to start singing as the 5 minutes we spent in total stillness I had been processing the rest of the day, and how it must have felt to be an actual soldier on the front lines. I also started feeling the weight of what we had done, remembering the dead in the most personal way possible, recreating them as living memorials.

The final song was the same as the one we'd been singing all day but with one final twist. Before we finish the song we interrupt it with a scream. We were encourage to use as much force as possible, to give the public as much of a shock as possible. The rest of the day the public has heard us singing the song and leaving a contemplative silence at the end, now we would be subverting that to give them a moment to remember.

After a short pause at the end of the song we all broke off through any available door and headed back to the Garrison in small groups. Our day was finished!

## Final Thoughts

This has been such an amazing experience. I really don't think it's hyperbole to say that this is going to be a lifelong memory for me, something that will never be topped. So much work went into this from so many people so I feel the need to thank everyone involved:

- Jeremy Deller - Creator
- Simon Sharkey - Director
- Brigid McCarthy - Movement Director
- Chris Grant - Associate Director
- Christopher Wright - Workshop Facilitator
- Dawn Taylor - Producer
- Karen Allan - Producer for the Event Day
- Lisa Ward - Stage and Project Manager
- Costume Supervisors
    - Cara McDiarmid
    - Letty Bishop
    - Ellie Coutts
    - Roo Callaghan
- Ashley Tulloch - Deputy Stage Manager
- Assistant Stage Managers
    - Lauren Doughton
    - Amanda Shearer
    - Sarah Thomson
    - Rosie Rowland
    - Lorna McKay
- Adam McDougall - Press and Marketing
- Emma Davis - First Aider

For a lot of those people the work isn't done yet! There are still costumes to organise and sent back, press stuff o organise and countless other thing that I'm probably not even fully aware of. I appreciate all of the dedication they've put into pulling this off, it was such an amazing experience.

I found out that our group walked a total of 19km on the day. That's not far off a half marathon! And we did it full uniform, made of wool, on a warm Shetland day, with old fashioned boots on. The muscle fatigue (or "spaegie" in Shetland parlance) I've been feeling since is unreal.

Finally, I would again like to thank everyone involved, and to express just how grateful I am to have been asked to be a part of such a massive and touching tribute to those who lost their lives at the battle of the Somme. We only mentioned those that died on the first day (and not even all of them, 19,240 British soldiers lost their lives), but this battle raged on until November 1916, thousands more died for our country before this battle ended.

If you find any mistakes in this post (which ended up being far larger than I ever anticipated) then please give me a shout on twitter (I'm [@{{ site.twitter.username }}](https://twitter.com/{{ site.twitter.username }})) and let me know. I still need to add some imagery to this, but there is already a ton of content online (search for #wearehere on [Facebook](https://www.facebook.com/search/top/?q=%23wearehere), [Instragram](https://www.instagram.com/explore/tags/WeAreHere/) and [Twitter](https://twitter.com/search?q=%23wearehere)).

Thanks for reading,
Matthew Simpson
