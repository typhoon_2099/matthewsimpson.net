---
layout: post
title: Carol - Therese
date: 2016-12-18 10:00:00
categories:
  - Blog
tags:
  - Art
  - Filmpoetry
  - Carol
  - Bruce Eunson
excerpt: Therese is anidder een wha comes ta be in a dwam whin missin hame
---

I had the pleasure of working with Bruce Eunson on a project of his called "Carol". This is the second of his videos, titled Therese. I had fun working on this and I'm really impressed with how it came out.

{% include vimeo.html id=196166660 %}
