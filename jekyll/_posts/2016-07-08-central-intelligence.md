---
layout: review
title: Central Intelligence - My Review
date: 2016-07-08 22:00:00
categories:
  - Blog
tags:
  - Review
  - Movie
image: /images/central_intelligence.jpg
image_alt: Central Intelligence
imdb: http://www.imdb.com/title/tt1489889/
youtube_id: MxEw3elSJ8M
---
I've just been to see [Central Intelligence at Mareel](http://www.mareel.org/watch/cinema-listings/film-central-intelligenc) so I thought I would give it a quick review.

First off, Dwayne Johnson is funny as hell. To date I have not seen him do a movie that he did not commit 100% to. In Fast and Furious he was a tough guy on a mission and it was believable. In Central Intelligence he's borderline psychotic and it's believable. I may have a thing for Dwayne Johnson, I'm not sure yet.

I think this may be Dwayne Johnson's first full comedy. He's cameoed in Reno 911: The Movie and the start of The Other Guys, but I don't think he's starred in a comedy (feel free to correct me on this via [Twitter](https://twitter.com/{{ site.twitter.username }})). He's done an excellent job with Central Intelligence, hopefully we'll get to see more of his funny side in the future.

Kevin Hart was also excellent. As an established comedian it would have been obvious for him to take the crazy character in this unlikely pairing. But instead he acts as the straight character and, despite his outbursts, comes across believable as the more reasonable character.

Overall the chemistry between Kevin Hart and Dwayne Johnson is fantastic. They work really well together on screen, and their posts on social media and promotional appearances seem to cement the fact that they are just two really nice guys who like each other.

The plot of the movie was well thought out. It wasn't just an excuse to get some silly jokes thrown in. Admittedly it's not some sort of masterpiece but with comedies it's easy to forget to write an interesting story in lieu of saying something stupid, so I'm grateful that there was some thought put into this. Plus it was an action movie too, so it needed some sort of story to follow.

I give this film a big thumbs up. It's a film I could quite easily watch again, either back at the cinema or once it's made it's inevitable way to Netflix. I'm also strongly considering buying a fanny pack and unicorn t-shirt then getting buff as hell for Halloween!
