---
layout: post
title: 50 Ways to Leave Your Lover
date: 2017-02-18 18:30:00
categories:
  - Comedy
tags:
  - Paul Simon
excerpt: Paul Simon famously sung that there's 50 ways to leave your lover. He never listed all 50, so I've taken the liberty to list them for him.
permalink: /:categories/:title/
---

{{ page.excerpt }}

1. Face to face
2. Phone call
3. Text message
4. Email
5. Normal mail (a.k.a. the "Dear John" letter)
6. Slide a letter under their student accommodation bedroom door on a Sunday morning then contradict yourself via text message (this happened once...)
7. Via a friend
8. Via an enemy
9. Via a frenemy
10. Through sky writing
11. Through semaphone
12. Through Smoke signals
13. Via Telegram
14. Via Sing-a-gram
15. Via Stripper-gram
16. Via Interpretive dance-a-gram
17. Put a notice in a newspaper
18. Put a full page advert in a newspaper
19. Publish a four page featurette in a newspaper
20. Create a D'agostini build your own breakup letter (60 issues, first issue 99p, other issues £8.99)
21. Declared by a Town crier
22. Declared by Jon Cryer
23. Over coffee
24. Over drinks
25. Over a shark tank
26. Written on a Sandwich board like funny signs outside of pubs that get posted to Reddit all the time
27. Written in the foam on the top of a Strbucks coffee
28. Written on the top of a Starbucks muffin
29. Written on a Big Mac
30. In a vlog
31. In a blog
32. In a Tweet
33. Update your Facebook status and wait for them to notice
34. Cut off all communication (a.k.a. "Ghosting")
35. Move to another city
36. Fake your own death
37. During an antenatal class
38. During an ultrasound
39. During labour
40. Write them a song
41. Write them a poem
42. Write them a haiku
43. Write a novel where the main theme is breakups and then ask them to write an essay on it
44. In a cryptic letter
45. In a letter written in elvish (if they can't read it it's further proof that you made the right decision)
46. In a crossword puzzle
47. In a word search
48. In a sudoku
49. In an anagram (Muddy Europe)
50. Turn off the life support machine

They're not all advisable to use but, if you're struggling to find a way to leave you lover then ask them to pick a number between 1 and 50 and do whatever it happens to be.
