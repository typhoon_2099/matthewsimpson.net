---
layout: post
title: "Modernising my Blog Part 2: Technologies"
date: 2020-05-11 12:00:00
categories:
  - Blog
tags:
  - Netlify
  - Jekyll
  - Sass
  - CSS
  - PostCSS
excerpt: Now that my tools are in order, it's time to introduce some fresh technologies to the browser
image: /images/fonts.jpg
---

## Use custom properties

Now that the build process is up to date, let's write some new code. I briefly mentioned CSS 
custom properties in the last post when I switched from Sass to PostCSS, but it's worth going 
into a bit 
more detail for those who aren't familiar with them.

Custom properties are a CSS way of defining reusable values, in a way that is supported natively 
in modern browsers. They are defined just like you would define a style in CSS, but they start 
with two hyphens (`--*`). They can then be accessed using the `var()` function. The preferred place
to define custom properties is on the `:root` psuedo selector. Once declared they can be used as
required and overridden further down the DOM using the correct selectors. For example:
  
```postcss
:root {
  --background: #00ff00;
}

.container {
  background-color: var(--background);
}

.container--dull {
  --background: #004400;
}
```

Now you can create a div with the `container` class for a horribly bright green background, and 
then create a second div with `container` and `container--dull` for something that won't burn out
your visitors retinas!

You can achieve similar things with the right classes, but custom properties come into their own 
when you start using them in more complex situations, such as in media queries. My main 
motivation for adding custom properties is to easily implement a dark mode for the site, so for 
now I have added the following custom properties, and used them where required:

```postcss
:root {
  --background: #ffffff;
  --text-color: #494451;
  --main-color: #673ab7;
  --compliment: #e4fa3d;
  --article-divider: #c6c2ce;
}
```

## Add dark mode

I love dark mode, I use it on any app or site that offers it. It reduces eye strain and on mobile
can save on battery life. What's not to love! Unfortunately, I haven't made the time to add it 
to my own site, so now's the time. Having defined all the colours on the site in custom 
properties it's incredibly simple to add a dark mode, as it's also supported by CSS in modern 
browsers. The `prefers-color-scheme` media feature is all we need to check what visitors prefer and
change the styling appropriately. For a simple site like this blog it's as simple as this:

```postcss
:root {
  --background: #ffffff;
  --text-color: #494451;
  --main-color: #673ab7;
  --compliment: #e4fa3d;
  --article-divider: #c6c2ce;
  @media (prefers-color-scheme: dark) {
    --background: #131117;
    --text-color: #d7cfe6;
  }
}

```

Done! The default will be a white backgrounded site (I'm almost certain that backgrounded isn't a
word, but I can't think of a better one) and people who prefer a dark colour scheme will get the a 
dark backgrounded site (nope, still can't think of a better word, backgrounded is real now). Some
other elements may need to recoloured with their own custom properties to ensure that they look nice
in dark mode. They can easily be added to the variables file.

If you want to try both versions then you can toggle dark mode in Chrome by changing the system 
preferences in Windows and Mac, and if you use Firefox you can switch to the dark theme.

## Use sticky nav bar

A long time ago I updated my header by implementing Luxbar. It was pretty simple to add to the 
site and handles responsive menu items by adding them to a dropdown menu, all without needing 
Javascript. Which is pretty cool. Something I want to tweak is to replace the absolute
positioning with sticky positioning. It's pretty simple, this:

```postcss
.luxbar-fixed {
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1000;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12),0 1px 2px rgba(0,0,0,0.24);
}
```

becomes this:

```postcss
.luxbar-fixed {
  position: sticky;
  top: 0;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
}
```

It's less styling to worry about, and less work to get it in the right place. Just tell it to be
sticky, and set `top: 0` to stick it to the top of the page. Sticky positioning keeps an element
in view while while its parent is still in view. Once it hits the bottom (or top) of the element it
scrolls away with the parent. This can be used for a number of things, such as table headings (ever
scrolled through a long table on Wikipedia on your phone and forgotten what column is what? Sticky
headers sorts that out!).

## Typography

Readability has been on my mind recently. I spent some time in the original design ensuring that
text was readable and pleasant but more could be done. Headings use a
[type scale](https://type-scale.com/) to look nice, but they're all one size too big, so bumping
each one down a size will improve things.

The font I chose for the main text body is also quite thin which, combined with a long line length,
makes for a pretty difficult read. There are lots of articles online which discuss line length, and
they all suggest slightly different answers, but they are all based on the number of characters per
line. I originally decided on 75, but I'm now realising that this was too high, and that my choice
of font has made things worse.

The width of the content is based on the height on the font (due to how font sizes are defined in
CSS), so a thin font means that you actually end up with far more than 75 characters per line. I can
fix this in one of two ways: I can reduce the width, or I can switch to a wider font (spoiler: I'm
going to do both).

Reducing the text width to `55rem` makes for a more readable line length (my opinion), and switching
from Roboto Condensed to Roboto solves the font width issue. I've also bumped the text up from 18px
to 20px to ensure that it's easily readable.

## Conclusion

Things are looking a lot better. Text is easier to read, and dark mode will reduce eye strain (and
battery consumption). I've also knocked out a fixed position element into a simpler stick element.
Now that the simple fixes have been put into place, I can tackle the next big part of the refresh:
Improving accessibility... 
