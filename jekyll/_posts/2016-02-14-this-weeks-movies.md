---
layout: post
title: This Week's Movies, 14th February 2016
date: 2016-02-14 17:00:00
categories:
  - Blog
---
I haven't posted for a while so I thought I'd do some quick reviews for the movies I've seen this week.

## Daddy's Home - Tuesday night @ [Mareel](http://www.mareel.org)

A comedy starring Will Ferrell as a step-father who comes face-to-face with his step-childrens' real father (Mark Whalberg) when he reenters their lives.

I thought this film was okay. There were some funny moments in the movie but if you've watched the trailer already then you've already seen them. The plot was nothing special but it did provide a fairly satisfying ending to the whole thing. I tend to enjoy Will Ferrell movies so I gave this film a pass but I don't think it had much else going for it. It's maybe worth a watch when it hits Netflix if you have nothing else to watch. Also, I like seeing [Linda Cardellini](http://www.imdb.com/name/nm0004802/) on screen so I was happy to see her in the movie but thought she could have possibly been used a bit more.

## Zoolander 2 - Friday night @ [Mareel](http://www.mareel.org)

This movie was really funny. I was worried that after 10 years of adoring the original {{ "B00005UO64" | amazon_product: 'Zoolander' }} movie that this film would end up being disappointing. I was ended up feeling relieved as it managed to keep me interested throughout. I have heard others say that the film relied too heavily on it's cameos, but I'm not sure I agree. While there are a lot of them in the movie, they are all used in a way that compliments the plot, rather than slowing it down.

I also thought the plot was pretty well done. It's these kinds of films that can sit back and rely on having returning characters to fire out one-liners but the plot helped to contain any callbacks into something that, like the cameos, benefited the plot rather than hinder it.

I'm also glad to see some Saturday Night Live alumni in the movie too. Considering the links the film has to SNL it's not surprising that there would be at least one or two involved (Ben Stiller appeared as a cast member for 4 episodes and Will Ferrell was a main cast member for many years), but it was nice to see Kyle Mooney playing Don Atari. I've been watching more of the recent episodes of SNL and it really seems to be on form at the moment, Kyle Mooney being one of the current cast members I really enjoy (I'm also super excited for the new [Ghostbusters](http://www.imdb.com/title/tt1289401/) film so I can see [Kate McKinnon](http://www.imdb.com/name/nm0571952/) on screen too).

## Dirty Grandpa - Saturday night @ [Mareel](http://www.mareel.org)

I can't really say much about this film. It was okay. The plot was a bit stupid (and I'm not even sure how it resolved the way it did) and most of the jokes revolved around Robert DeNiro's character making offensive comments.

There was an odd thread in the story where Dirty Grandpa (I didn't take the time to commit his name to memory) makes fun of a gay black man for being gay/black, which barely pays off a bit later in the film when he comes to his aid. The resolution of this was fairly obvious as soon as it begins, but there was no real indication that this was where it was going from the first two incidents (it genuinely comes off as Dirty Grandpa's character being needlessly homophobic). The whole thing left a bit of a bitter taste in my mouth.

Aubrey Plaza's character was funny enough but again a lot of the humour was based around her wanting to hook up with Dirty Grandpa. While I'm not adverse to jokes like this I felt like there were just too many to be effective.

I did enjoy [Jason Mantzoukas](http://www.imdb.com/name/nm1727621/) as Pam, the surf shop owner/drug dealer. He tends to play similar characters in his roles (see Rafi on The League) but he plays them well, so I don't care too much.

If you want to see Robert DeNiro drop some N-bombs, or you want to see Zack Efron's ridiculously sculpted body (seriously, what's his routine?), or see Aubrey Plaza in a bikini then give it a watch. Otherwise, it's probably not worth it.

## How to be Single - Sunday afternoon @ [Mareel](http://www.mareel.org)

This film took me by surprise. I was expecting a funny film with a fairly generic plot line to follow along to. What I ended up seeing was a much more intricate movie which seemed to genuinely explore the various ways that people end up being single, and how they deal with their situation. The movie managed to keep surprising me as just I thought I knew what was about to happen it would move in a different direction.

The four main leads ([Rebel Wilson](http://www.imdb.com/name/nm2313103/), [Dakota Johnson](http://www.imdb.com/name/nm0424848/), [Leslie Mann](http://www.imdb.com/name/nm0005182/) and [Alison Brie](http://www.imdb.com/name/nm1555340/)) gave great performances and I'm hoping Alison Brie will move on to bigger and bigger projects (I've wanted to see her do well since Community). I didn't think Nicholas Braun was particularly amazing, his performance was capable but he didn't blow me away either. He never really seemed to be that invested in the people he supposedly cared for.

Again, Jason Mantzoukas was great. He didn't have too much screen time but when he did he was pretty funny.

I enjoyed the variation in the characters, they all had different viewpoints on life and relationships and this is used to great aplomb in the movie. I found out at the end of the movie that it's based on a {{ "1471146634" | amazon_product: 'book of the same name' }}. This isn't surprising as it was very well written and I find that movies based on books end up being pretty decent (Mean Girls springs to mind). The end of the film is an emotional one, so if you're going to give this a watch then you might want to bring tissues with you.

This film is really funny and has a great story at it's underpinning. I'd recommend seeing this film, either by yourself or with your significant other (if you have one).

### Wrap Up
Okay, that's enough mind dumping from me, hopefully this is interesting to someone, at least for me it's an exercise in trying to write more (I'd like to get better at being able to note down my idea/opinions so be prepared for more random posts like this).
