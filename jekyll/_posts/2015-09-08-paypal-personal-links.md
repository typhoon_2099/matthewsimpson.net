---
layout: post
title: "Personal PayPal Links"
excerpt: "PayPal have released a new link system so you can send people links for quick payments"
date: 2015-09-08 17:00:00
categories:
  - Blog
---
I've just bagged myself one of PayPal's new [paypal.me](https://www.paypal.me) links, and I got to use my real name!

I must have been quick as my name seems to get used for everything else pretty quickly (I'm looking at you [matthewsimpson.com](http://www.matthewsimpson.com) and [matthewsimpson.co.uk](http://www.matthewsimpson.co.uk)!)

It seems pretty cool, I can send my link to people [https://www.paypal.me/MatthewSimpson](https://www.paypal.me/MatthewSimpson) and then they can enter what they want to send me. Even better, I can add a value to the end to preset the amount (such as this: [https://www.paypal.me/MatthewSimpson/3.14](https://www.paypal.me/MatthewSimpson/3.14)).

I can see me using this plenty in the future when I'm trying to organise trips with friends or when I'm trying to sell things privately online, and also when people send me their links too.

[Feel free to send me £1!](https://www.paypal.me/MatthewSimpson/1)
