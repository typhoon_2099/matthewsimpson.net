---
layout: review
title: Love and Friendship - My Review
date: 2016-07-06
categories:
  - Blog
tags:
  - Review
  - Movie
image: /images/love_and_friendship.jpg
image_alt: Central Intelligence
imdb: http://www.imdb.com/title/tt3068194/
youtube_id: 8MaSK3POHI0
---
I've just been to see [Love and Friendship at Mareel](http://www.mareel.org/watch/cinema-listings/film-love-and-friendship/) so I thought I would give it a quick review.

First thing I noticed about this movie: It's rated "U"! I didn't think they still released movies with "U" ratings, I couldn't tell you the last time I saw one at the cinema (that's a lie, I keep all my cinema tickets, it was Labyrinth back in March). It kind of surprised me, I assumed that the film would contain some sort of lewd conduct in it. I was wrong, and the film did fantastically without having to resort to such measures.

The movie revolves around Lady Susan Vernon, played by Kate Beckinsale, who, recently widowed, seeks to find a husband for her daughter, Frederica (Morfydd Clark), as well as herself. She invites herself to her in-laws estate and begins her plan to wed Frederica to wealthy "blockhead" James Martin, as well as escape from the rumours of her reputation.

I enjoyed this movie. The story was always interesting, Lady Susan kept me on my feet as she managed to talk her way out of most situations. Even when it seems like she was going to get rumbled for manipulating people she would manage to bring them back to her side. I did think that the ending was slightly abrupt, everything gets wrapped up pretty suddenly. However it doesn't detract from the rest of the movie, so I don't see this as a huge downside.

James Martin was a wonderfully funny character. His total ineptitude would have been very easy to fall too much into some sort of silly slapstick, but thankfully this does not happen and you end up with a character that is entertaining to watch as he shows just how stupid he is. It's also funny to notice that he doesn't seem to realise just how stupid he is (everyone else notices all to well).

I would recommend seeing this movie if you get the chance (Mareel will be showing it again this Sunday the 10th of July). It's a clever film that's wonderfully acted and doesn't fail to entertain.
