---
layout: post
title: "Moving my site from DigitalOcean to Netlify"
date: 2017-06-11 15:00:00
categories:
  - Blog
tags:
  - Netlify
  - Jekyll
  - Git
  - BitBucket
  - GitHub
  - GitLab
excerpt: It turns out that moving my Jekyll site over to Netlify takes no time at all, here's how
---

[Netlify's](https://www.netlify.com) ease of use is so good that I managed to transfer my blog over to it in a few minutes. I'll explain the process and keep an update on the time it took to sign up, build my site and then publish it on the correct domain.

I was waiting for a task to run a while ago and my mind started wandering. I'd remembered that I'd been looking into Netlify as an option for continuous deployment of statically generated sites, which could come in useful for a few projects I've been doing at work, so I jumped over to the site to check it out. **[0m 0s]**

I wasn't ready to host any of the work projects yet so I figured I'd just sign up with my own details and try it out on the repo for this blog. At the time the site was built on a [DigitalOcean](https://www.digitalocean.com/) droplet by pushing changes to a deploy origin and adding in post-receive hooks to call the build commands. It was a fair bit of work to set up and it infrequently didn't work (file permissions, incorrect gem versions, that sort of thing). Netlify should reduce the work I have to do and allow me to get on which what I'm trying to do instead. It's also backed by a CDN so I should even be able to improve the speed of my site.

Another possible alternative could have been [GitHub Pages](https://pages.github.com/), but I host my repos on [BitBucket](https://bitbucket.org/), plus GitHub Pages doesn't allow for third party Gems to be used in the [Jekyll](https://jekyllrb.com/) builds.

The sign up process for [Netlify](https://www.netlify.com) allows you to sign up using [BitBucket](https://bitbucket.org/), [GitHub](https://github.com/), [GitLab](https://about.gitlab.com/) or plain old email so, since I was already logged in, I chose BitBucket and authorised the application.  **[0m 10s]**

Since I had authorised access to [BitBucket](https://bitbucket.org/) the next screen asked me which BitBucket repo I wanted to deploy as my first site. I picked the repo, then chose my master branch, and Netlify picked on my repo being a [Jekyll](https://jekyllrb.com/) site so it had already filled out the build command and publish directory fields correctly.  **[0m 18s]**

Once the details were in I hit the go button and waited a moment while the site was generated (so now I'm waiting on two tasks to finish!) and, once it was done, I was given a Netlify subdomain to go and view the site. That was it, the site was live and ready to use. I compared the newly generated site to the version on my [DigitalOcean](https://www.digitalocean.com/) droplet and of course there was no difference, it was just there and ready to view.  **[1m 20s]**

Now that the site was deployed to Netlify I only had to do a couple of other things to replace my droplet site and start using Netlify full time. First, I has to replace my www A record with a CNAME for the randomly assigned subdomain Netlify gave me (you can change this to something more memorable while you're not ready to go live with a site), and update the root A record to point to Netlify's IP address (which allows for www redirects). **[2m 40s]**

Secondly, once the DNS records had propagated, I had to issue an SSL certificate. This does take a moment but my TTL on my DNS records was pretty low before I started, so it didn't take long. Netlify handles everything, you just need to press the button and then once it's done you can enforce it to redirect HTTP requests. **[4m 30s]**

And we're done! A little under 5 minutes was all it took for me to get my site not just up and running but confiured the way I want it to be accessed. I should also note that I'm on the free tier that Netlify offers, and I can do everything I want to do for this little site. If you need to be able to modify headers, do prerendering or configure redirects then you'll need to pay a monthly subscription.

[Netlify's](https://www.netlify.com) Pro plan ($49/month at time of writing) allows for branch-based deploys and testing on custom urls. You can get some of this functionality by creating a new site in Netlify and pointing it to another branch on the same repo. I do this with my site's master and dev branches so I can test out new features before launching them to the main site (did I mention that you can roll back to older builds in Netlify if you make some sort of gargantual mistake?). The only caveat to this is that you will need to push your dev branch before merging and pushing the master branch. If both branches change in the same commit then Netlify seems to get confused with the webhooks it watches.

The final thing I wanted to point out that's quite useful is the option to receive notifications when builds are started/finshed/failed. You can get them either via email or through a Slack channel, or through webhooks or [BitBucket](https://bitbucket.org/) notifications/pull request comments. It's a neat feature which I've only scratched the surface on.

Anyway, that's how I got my site up and running in a few minutes. You know how to do it, go and give it a shot!
