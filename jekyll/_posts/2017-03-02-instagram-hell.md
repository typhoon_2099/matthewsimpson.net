---
layout: post
title: "My Instagram Hell or: How I Learned To Stop Worrying and Wind Up Help Center Robots"
date: 2017-03-02 19:00:00
categories:
  - Comedy
tags:
  - Instagram
  - Shetland Comedy
  - Totally Real Banana
excerpt: It turns out it's incredibly difficult to get an Instagram account reactivated...
permalink: /:categories/:title/
---

I was going to write a longer post about how I came to the position where I was locked out of two Instagram accounts but then I realised it would be better to get to the good stuff (side note: If you get locked out of an Instagram account you won't be able to use the instructions in the app because the image upload bit is broken, use [this link](https://help.instagram.com/contact/1652567838289083) instead. Thank me later beacuse this doesn't come up in the search results on their website and I only happened to stumble upon it while Googling frantically for an answer. I also just spent another 20 minutes trying to find that page again. Argh!!!!!!).

Anyway, I got given the runaround by the Instagram system, due to my using email forwarders instead of an individual email account, and due to the wording of the page (Shetland Comedy isn't really a "business, product or service" but it seems to have been the solution). The email I was sent read as follows:

> Hi,  
> Thanks for contacting us. Before we can help, we need you to confirm that you own this account.
>
> Please reply to this email and attach a photo of yourself holding a handwritten copy of the code below.
>
> 390035613
>
> Please make sure that the photo you send:
>
> - Includes the above code, handwritten on a clean sheet of paper, followed by your full name and username
> - Includes both the hand that is holding the sheet of paper and your entire face
> - Is well-lit and is not too small, dark or blurry
> - Is attached to your reply as a JPEG file
>
> Bear in mind that, even if this account doesn't include any pictures of you or is used to represent someone or something else, we won't be able to help you until we’ve received a photo that meets these requirements.
>
> Thanks,  
> The Instagram Team

I replied to this email stating my email forwarder issues (and that I couldn't reply from the correct address) and got this reply:

> Hi,  
> It doesn't look like we can help you with the problem you're having from here. We're sorry for the inconvenience.
>
> If you're still looking for help with this, the Help Center is a great place to find answers to frequently asked questions and up-to-date forms you can use to get in touch with us:
>
> https://www.facebook.com/help
>
> Thanks,  
> The Facebook Team

The Facebook team. Not Instagram, Facebook. I figured I wasn't going to get anywhere with this so I decided to do a bit of messing around. I replied again with the following email:

> Hello,  
> Please find attached the required image of myself holding a sheet of paper with the required text, my full face and my hand. To ensure the reactivation process is as smooth as possible I have also included a copy of a recently published local newspaper, as well as the other side of my hand (which was preoccupied with holding the paper in the first image) and a picture of my full hand holding my full face, just in case I have misinterpreted the original request.
>
> Please let me know if you require any further details, I have a full driving license, passport, a recent electricity bill, my birth certificate, my death certificate (don't ask) and a signed photograph of Tina Fey (which probably isn't too helpful but I like to show it off when I can).
>
> Kindest Regards,  
> Matthew Simpson  
> Shetland Comedy  
> Totally Real Banana
>
> {%
  picture insta_post /images/instagram/1.jpg --alt 'My unhappy face'
%}{%
  picture insta_post /images/instagram/2.jpg --alt 'My hand'
%}{%
  picture insta_post /images/instagram/3.jpg --alt 'My hand holding my face'
%}

I don't look to pleased in the first photo. I genuinely wasn't. This was the reply:
I don't look to pleased in the first photo. I genuinely wasn't. This was the reply:

> Hi,  
> It doesn't look like we can help you with the problem you're having from here. We're sorry for the inconvenience.
>
> If you're still looking for help with this, the Help Center is a great place to find answers to frequently asked questions and up-to-date forms you can use to get in touch with us:
>
> https://www.facebook.com/help
>
> Thanks,  
> The Facebook Team

As expected, a robot reply. But hang on, what if it's not? I looked at the photos and realised I hadn't followed all the guidelines properly, so I sent a follow up email:

> Hello,  
> Apologies for the last email I sent, I realise that the photo I sent you for proof was not that well lit (despite that being clearly defined in your original email!!!) so I have attached a new image with better lighting. I actually quiet like this photo of me, I may use it as my profile picture! (after you've approved me, of course).
>
> Regards,  
> Matthew Simpson  
> Shetland Comedy  
> Totally Real Banana
>
> {% picture insta_post /images/instagram/4.jpg --alt 'A far more well lit photo' %}

Now to play the waiting game and hope that I had been successful. Nope:

> Hi,  
> It doesn't look like we can help you with the problem you're having from here. We're sorry for the inconvenience.
>
> If you're still looking for help with this, the Help Center is a great place to find answers to frequently asked questions and up-to-date forms you can use to get in touch with us:
>
> https://www.facebook.com/help
>
> Thanks,  
> The Facebook Team

I was on the verge of giving up when I realised I was still coming up short on the recommendations for the image, it's no wonder Amanda (that's what I'm calling the potential human on the other end of this conversation) had rejected my previous submissions. No problem, I'm a GIMP wizard! I'll just touch up and resize the image and be on my merry way, posting my expertly touched up photos onto Instagram in a jiffy!

> Hello yet again,
>
> I can't believe it's taken me three attempts to read the confirmation instructions properly! I've read over it carefully and I've gone back to the first image and adjusted it to meet your expectations. I've lightened up the background (and made it more tropical!!!) as well as sharpening up the image, to make sure the handwriting on the sheet of paper is as clear as possible. You'd think I've been hitting the gym with the definition going on in this picture!
>
> This one's a definite winner for my new Tinder profile picture, how could anyone turn me down?! (I mean, apart from Instagram, twice.)
>
> Glad to have this settled,  
> Matthew Simpson  
> Shetland Comedy  
> Totally Real Banana
>
>
> {% picture insta_post /images/instagram/5.jpg --alt 'Clearly this is going to work' %}

There's no second guessing now, either it's accepted and I get into my accounts, or I'm definitely talking to a robot:

> Hi,  
> It doesn't look like we can help you with the problem you're having from here. We're sorry for the inconvenience.
>
> If you're still looking for help with this, the Help Center is a great place to find answers to frequently asked questions and up-to-date forms you can use to get in touch with us:
>
> https://www.facebook.com/help
>
> Thanks,  
> The Facebook Team

Must be a robot.

## Epilogue

The eventual answer to my problem was to treat both accounts like a company, service or product and send a screenshot of the email receipt for the domain names I purchased. Somehow that's an acceptable form of identification, something that can be easily doctored. At least now you can follow Shetland Comedy on Instagram ([ShetlandComedy](https://www.instagram.com/shetlandcomedy/)).
