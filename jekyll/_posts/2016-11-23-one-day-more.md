---
layout: post
title: One Day More (To Aberdeen)
date: 2016-11-23 01:00:00
categories:
  - Comedy
tags:
  - Parody
  - Music
  - Les Misérables
  - Aberdeen
  - Northlink Ferries
  - Jean Valjean
  - Shetland
excerpt: My parody of One Day More from Les Misérables
image: /images/one_day_more.jpg
image_alt: One Day More
permalink: /:categories/:title/
---

VALJEAN  
One day more  
Another day, another destiny  
This never-ending boat to Aberdeen  
The men with which a share this bunk  
Will surely turn up blazing drunk  
One day more

MARIUS  
I did not get a room today  
Could I get one once we've departed

VALJEAN  
One day more

MARIUS & COSETTE  
Tomorrow we won't stay awake  
Because we're stuck with these recliners  

EPONINE  
One more day until I'm home

MARIUS & COSETTE  
Will we ever sleep again

EPONINE  
Too much fog to fly from Sumburgh

MARIUS & COSETTE  
And there were no 2-berth rooms

EPONINE  
What a flight I might have known

MARIUS & COSETTE  
So we'll just have to make do

EPONINE  
But the plane just couldn't leave

ENJOLRAS  
One more day before the storm

MARIUS    
Will the boat get in on time

ENJOLRAS      
It's a force 9 south-easterly

MARIUS  
Will we have to leave at 3

ENJOLRAS  
When the weather starts to form

MARIUS  
But I have onward travel plans

ENJOLRAS  
Will we get stuck in Orkney

ALL  
The time is now, the boat is here

VALJEAN  
One day more!

JAVERT  
One more day until the mainland  
Then we carry on by car  
It's a family excursion  
But I'll be drinking at the bar

VALJEAN  
One day more

M. & MME. THENARDIER  
Watch 'em run amuck  
Catch 'em as they fall  
Children roam the Northlink  
like a free for all  
Games in the arcade  
No restraints as such  
Most of them have parents  
But they don't do much

STUDENTS (2 Groups)  
1: One day till our Christmas shopping  
2: Union Square is beckoning  
1: We can get a Burger King  
2: Or maybe get KFC  
1: We should take back some Krispie Kremes  
2: If we're not feeling too sick

ALL  
Do you hear the people spew

MARIUS  
My place is here, I vomit too

VALJEAN  
One day more!

MARIUS & COSETTE  
I did not get a room today

EPONINE  
One more day until I'm home

MARIUS & COSETTE  
Could I get one once we've departed

JAVERT (overlapping)  
I will join the people drinking  
I'll down Tennant's by the pint  
I will overstay my welcome  
And I'll try to start a fight

VALJEAN  
One day more

MARIUS & COSETTE  
Tomorrow we won't stay awake

EPONINE  
What a flight I might have known

MARIUS & COSETTE  
Because we are stuck with recliners

JAVERT (overlapping)  
One more day until the mainland  
Then we carry on by car  
It's a family excursion

THE NARDIERS (overlapping)  
Watch 'em run amuck  
Catch 'em as they fall  
Children roam the Northlink  
like a free for all

VALJEAN  
Tomorrow we'll be far away  
Tomorrow is the judgement day

ALL  
Tomorrow we'll discover  
What the Scottish Mainland has in store  
One more dawn  
One more day  
One day more
