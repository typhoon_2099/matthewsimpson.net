---
title: Welcome
excerpt: Welcome to Matthew Simpson.net
layout: default
permalink: /
---

Hello, here's my blog where I infrequently post things that nobody cares about. The subjects vary wildly so be prepared for whatever unusual information I have decided to share.

## Latest Blog Posts

{% include posts.html %}
