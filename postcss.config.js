const purgecss = require('@fullhuman/postcss-purgecss');

module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-preset-env')({
      features: {
        'nesting-rules': true,
      },
    }),
    // purgecss({
    //   content: ['./jekyll/**/*.html'],
    // }),
  ],
};
